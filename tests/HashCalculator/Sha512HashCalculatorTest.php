<?php
declare(strict_types=1);

namespace N11t\HashFileService\HashCalculator;

use PHPUnit\Framework\TestCase;

class Sha512HashCalculatorTest extends TestCase
{

    public function test()
    {
        // Arrange
        $content = 'Hello World';
        $calucator = new Sha512HashCalculator();

        // Act
        $actualHash = $calucator->calculateHash($content);

        // Assert
        $expectedHash = '2c74fd17edafd80e8447b0d46741ee243b7eb74dd2149a0ab1b9246fb30382f27e853d8585719e0e67cbda0daa8f51671064615d645ae27acb15bfb1447f459b';
        self::assertSame($expectedHash, $actualHash);
    }
}
