<?php
declare(strict_types=1);

namespace N11t\HashFileService\HashCalculator;

use PHPUnit\Framework\TestCase;

class GenericHashCalculatorTest extends TestCase
{

    public function providerAlgorithm(): array
    {
        $data = 'Hello World';

        $result = [];
        foreach (hash_algos() as $algo) {
            $result[] = [$algo, $data, hash($algo, $data, false)];
        }

        return $result;
    }

    /**
     * @dataProvider providerAlgorithm
     *
     * @param string $algo
     * @param string $content
     * @param string $expectedHash
     */
    public function test(string $algo, string $content, string $expectedHash)
    {
        // Arrange
        $calculator = new GenericHashCalculator($algo);

        // Act
        $actualHash = $calculator->calculateHash($content);

        // Assert
        self::assertSame($expectedHash, $actualHash);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Given algorithm is invalid.
     */
    public function testWillThrowExceptionOnInvalidAlgorithm()
    {
        new GenericHashCalculator('asdf');
    }
}
