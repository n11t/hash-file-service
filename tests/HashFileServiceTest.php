<?php
declare(strict_types=1);

namespace N11t\HashFileService;

use N11t\HashFileService\HashCalculator\Sha1HashCalculator;
use N11t\PHPUnit\DirectoryTestCase;

class HashFileServiceTest extends DirectoryTestCase
{

    /**
     * {@inheritdoc}
     */
    protected function getAssetsDirectory(): string
    {
        return __DIR__ . '/Assets';
    }

    public function testCanCreateService()
    {
        // Act
        $service = $this->createService();

        // Assert
        self::assertNotNull($service);
    }

    public function testCanCreateDirectoryIfNotExists()
    {
        // Arrange
        $unitTestDirectory = $this->getPath('/');
        $expectedDirectory = $unitTestDirectory . 'directoryToCreate';

        // Act
        new HashFileService($expectedDirectory, new Sha1HashCalculator());

        // Assert
        self::assertDirectoryExists($expectedDirectory);
    }

    public function testCanCreateDirectoryRecursivelyIfNotExists()
    {
        // Arrange
        $unitTestDirectory = $this->getPath('/');
        $expectedDirectory = $unitTestDirectory . 'directoryToCreate/recursive';

        // Act
        new HashFileService($expectedDirectory, new Sha1HashCalculator());

        // Assert
        self::assertDirectoryExists($expectedDirectory);
    }

    public function testCanPersistByContent()
    {
        // Arrange
        $content = $this->assetGetContent('lorem-ipsum-10-paragraph.txt');

        $directory = $this->getPath('/service');
        $service = $this->createService();

        // Act
        $filename = $service->persistByContent($content);

        // Assert
        $content = $this->assetGetContent('lorem-ipsum-10-paragraph.txt');
        $expectedFileName = $directory . '/88/d8155a8a9f6734ccf1023f6a3b85426fe771c1';

        self::assertSame($expectedFileName, $filename);
        self::assertStringEqualsFile($expectedFileName, $content);
    }

    public function testCanPersistByFile()
    {
        // Arrange
        $assetFile = $this->copyAsset('lorem-ipsum-10-paragraph.txt', 'file.txt');

        $directory = $this->getPath('/service');
        $service = $this->createService();

        // Act
        $file = $service->persistByFile($assetFile);

        // Assert
        $content = $this->assetGetContent('lorem-ipsum-10-paragraph.txt');
        $expectedFileName = $directory . '/88/d8155a8a9f6734ccf1023f6a3b85426fe771c1';

        self::assertSame($expectedFileName, $file);
        self::assertStringEqualsFile($expectedFileName, $content);

    }

    public function testCanPersistByResource()
    {
        // Arrange
        $assetFile = $this->copyAsset('lorem-ipsum-10-paragraph.txt', 'file.txt');
        $handle = \fopen($assetFile, 'rb+');

        $directory = $this->getPath('/service');
        $service = $this->createService();

        // Act
        $file = $service->persistByResource($handle);
        \fclose($handle);

        // Assert
        $content = $this->assetGetContent('lorem-ipsum-10-paragraph.txt');
        $expectedFileName = $directory . '/88/d8155a8a9f6734ccf1023f6a3b85426fe771c1';

        self::assertSame($expectedFileName, $file);
        self::assertStringEqualsFile($expectedFileName, $content);
    }

    public function createService(): HashFileService
    {
        $directory = $this->createDirectory('/service');

        return new HashFileService($directory, new Sha1HashCalculator());
    }
}
