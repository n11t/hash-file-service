[![pipeline status](https://gitlab.com/n11t/hash-file-service/badges/master/pipeline.svg)](https://gitlab.com/n11t/hash-file-service/commits/master) [![coverage report](https://gitlab.com/n11t/hash-file-service/badges/master/coverage.svg)](https://gitlab.com/n11t/hash-file-service/commits/master)

# HashFileService

Service to persist files based on there hashed content

## Sample

Use the service to persist content or files.

### Code
```php
<?php

$calculator = new \N11t\HashFileService\HashCalculator\GenericHashCalculator('sha1');

$directory = sys_get_temp_dir() . '/hash-service';
$service = new \N11t\HashFileService\HashFileService($directory, $calculator);

$content = "Hello World\n";
echo $service->persistByContent($content);
echo PHP_EOL;

$file = sys_get_temp_dir() . '/test.txt';
echo $service->persistByFile($file);
echo PHP_EOL;

$handle = fopen($file, 'rb+');
echo $service->persistByResource($handle);
echo PHP_EOL;
```

### Output

```
/tmp/hash-service/2e/a47907b71df53d55ff2d8e5d6c0da6e6a39c84
/tmp/hash-service/2e/a47907b71df53d55ff2d8e5d6c0da6e6a39c84
/tmp/hash-service/2e/a47907b71df53d55ff2d8e5d6c0da6e6a39c84
```


