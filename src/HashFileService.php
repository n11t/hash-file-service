<?php
declare(strict_types=1);

namespace N11t\HashFileService;

use N11t\HashFileService\HashCalculator\HashCalculator;

class HashFileService
{

    /**
     * @var string
     */
    private $directory;

    /**
     * @var HashCalculator
     */
    private $hashCalculator;

    public function __construct(string $directory, HashCalculator $hashCalculator)
    {
        $this->createDirectoryIfNotExists($directory);

        $this->directory = $directory;
        $this->hashCalculator = $hashCalculator;
    }

    /**
     * Create a a directory.
     *
     * @param string $directory The absolute path to create.
     * @param bool $recursive Create the directory recursively.
     * @param int $mode The mode to set.
     * @return bool True if the directory was created. False otherwise.
     */
    private function createDirectoryIfNotExists(string $directory, $recursive = true, $mode = 0755): bool
    {
        return is_dir($directory) || mkdir($directory, $mode, $recursive) || is_dir($directory);
    }

    /**
     * Persist content in file. Based on the hash value.
     *
     * @param string $content The content to persist.
     * @return string The absolute path to created file.
     */
    public function persistByContent(string $content): string
    {
        $hash = $this->hashContent($content);

        return $this->writeContent($hash, $content);
    }

    /**
     * Persist file based on the content hash.
     *
     * @param string $file The absolute path to file.
     * @return string The absolute path to created file.
     */
    public function persistByFile(string $file): string
    {
        $content = \file_get_contents($file);

        return $this->persistByContent($content);
    }

    /**
     * Persist file based on given resource.
     *
     * @param resource $handle The resource to read from.
     * @return string The absolute path to created file.
     */
    public function persistByResource($handle): string
    {
        $content = stream_get_contents($handle);

        return $this->persistByContent($content);
    }

    /**
     * Calculate hash with {@see hashCalculator}.
     *
     * @param string $content The content to hash.
     * @return string The hash.
     */
    private function hashContent(string $content): string
    {
        return $this->hashCalculator->calculateHash($content);
    }

    /**
     * Persist content in file based on the given hash.
     *
     * @param string $hash The hash to persist the content in.
     * @param string $content The content to write in the file.
     * @return string The absolute path to file.
     */
    private function writeContent(string $hash, string $content): string
    {
        $path = $this->getPathForHash($hash);
        $filename = $this->getFilenameForHash($hash);

        if (!$this->createDirectoryIfNotExists($path)) {
            throw new \RuntimeException(sprintf('Unable to create directory %s', $path));
        }

        $filepath =  $path . '/'. $filename;

        if (!file_put_contents($filepath, $content)) {
            throw new \RuntimeException(sprintf('Unable to write file %s', $filepath));
        }

        return $filepath;
    }

    /**
     * Get the absolute path for given hash.
     *
     * @param string $hash The hash.
     * @return string The absolute path
     */
    private function getPathForHash(string $hash): string
    {
        $folder = substr($hash, 0, 2);

        return $this->directory . '/' . $folder;
    }

    /**
     * Get the filename for given hash.
     *
     * @param string $hash The hash.
     * @return string The filename
     */
    private function getFilenameForHash(string $hash): string
    {
        return substr($hash, 2);
    }
}
