<?php
declare(strict_types=1);

namespace N11t\HashFileService\HashCalculator;

class Sha512HashCalculator extends GenericHashCalculator
{

    public function __construct()
    {
        parent::__construct('sha512');
    }
}
