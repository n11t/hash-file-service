<?php
declare(strict_types=1);

namespace N11t\HashFileService\HashCalculator;

interface HashCalculator
{

    /**
     * Calculate the hash for given content.
     *
     * @param string $content The content to hash.
     * @return string The hash
     */
    public function calculateHash(string $content): string;
}
