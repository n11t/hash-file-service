<?php
declare(strict_types=1);

namespace N11t\HashFileService\HashCalculator;

class GenericHashCalculator implements HashCalculator
{

    /**
     * @var string
     */
    private $algo;

    /**
     * GenericHashCalculator constructor.
     * @param string $algo See {@see \hash_algos} for possible values.
     */
    public function __construct(string $algo)
    {
        $this->validateAlgorithm($algo);

        $this->algo = $algo;
    }

    /**
     * Validate if given algorithm is in the available algorithms.
     *
     * @param string $algo The algorithm to validate.
     */
    private function validateAlgorithm(string $algo): void
    {
        if (!\in_array($algo, hash_algos(), $strict = true)) {
            throw new \InvalidArgumentException(sprintf('Given algorithm is invalid. '));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function calculateHash(string $content): string
    {
        return \hash($this->algo, $content, $rawOutput = false);
    }
}
