<?php
declare(strict_types=1);

namespace N11t\HashFileService\HashCalculator;

class Sha1HashCalculator implements HashCalculator
{

    /**
     * {@inheritdoc}
     */
    public function calculateHash(string $content): string
    {
        return \sha1($content);
    }
}
